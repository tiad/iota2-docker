#!/usr/bin/env sh

usermod -u "${UID}" iota
groupmod -g "${GID}" iota
# check if $@ configuration file exists in the workspace
# let the user put /home/user/data/Iota_data_test/Exemple.cfg and replace with
# /mnt/workspace/Exemple.cfg
. /data/iota2/scripts/install/prepare_env.sh
su-exec iota python "${IOTABIN}" "$@"
