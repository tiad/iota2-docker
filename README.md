# iota2-docker

This repository contains a Dockerfile that can be used to automatically build a
working [iota2](https://framagit.org/inglada/iota2) image.

## Installation

1. Clone the repository: `git clone https://framagit.org/nicodebo/iota2-docker`
2. Build the image: `cd iota2-docker && docker build --tag=iota .` (This is going to
   take a few hours)

## Quick start

1. Download the Cesbio dataset mentionned in [Iota2's
  documentation](https://framagit.org/ArthurV/iota2-doc): [dataset](http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/IOTA2_TEST_S2.tar.bz2) and unzip it.

2. Modify the `IOTA2_Example.cfg` configuration file paths as follow:

    ```
    chain:
    {
        outputPath:'/mnt/workspace/IOTA2_Outputs/Results'
        remove_outputPath:True
        pyAppPath:'/data/iota2/scripts'

        nomenclaturePath:'/mnt/workspace/nomenclature.txt'

        listTile:'T31TCJ'
        S2Path:'/mnt/workspace/sensor_data'

        groundTruth:'/mnt/workspace/vector_data/groundTruth.shp'
        dataField:'CODE'
        spatialResolution:10
        colorTable:'/mnt/workspace/colorFile.txt'
    }

    argTrain:
    {
        classifier :'rf'
        options :' -classifier.rf.min 5 -classifier.rf.max 25 '
    }

    argClassification:
    {
        classifMode : 'separate'
    }

    GlobChain:
    {
        proj : "EPSG:2154"
    }
    ```

3. Launch the processing chain

    ```
    docker run \
        --rm \
        -v /dir/to/IOTA2_TEST_S2:/mnt/workspace \
        iota -config /mnt/workspace/IOTA2_Example.cfg
    ```

* `--rm`: Automatically remove the container when it exits
* `-v /dir/to/IOTA2_TEST_S2:/mnt/workspace`: mount the directory `/dir/to/IOTA2_TEST_S2` of
  the host into the directory `/mnt/workspace` of the container. Don't modify
  the right hand side !
* `iota`: this is the name of the image that you just build. `$ docker images`
  should list your locally available images.
* `-config /mnt/workspace/IOTA2_Example.cfg`: arguments of the Iota.py
  entrypoint.

Every paths (configuration file and arguments of the entrypoint should be
relative to the container. For exemple `/dir/to/IOTA2_TEST_S2/IOTA2_Example.cfg` on the host is
`/mnt/workspace/IOTA2_Example.cfg` on the container.

## Permission issues

File permission issues may arise if the default `user id (1000)` and `group id (1000)` of the container does not match the current user id and group id of the host.
To fix this, you can modify the `uid` and `gid` under which the iota processing
chain will be launched as follow:

```
docker run \
    --rm \
    -e UID="1003" \
    -e GID="1004" \
    -v /dir/to/data:/mnt/workspace \
    iota -config /mnt/workspace/IOTA2_Example.cfg
```
You can find out your host user id and group id with the following command: `$ id`
